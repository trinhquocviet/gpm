let dashboard = new Object;

dashboard.campaignTable = null;

dashboard.RegisterDateMask = function () {
  $('[name="send_date"]').inputmask();

  // $('[name="cost"]').inputmask();

}

dashboard.RegisterCampaignTable = function () {
  dashboard.campaignTable = $('#campaign-list').DataTable({
    "processing": true,
    "autoWidth": false,
    "orderable": false,
    ajax: {
      url: '/api/campaign',
      dataSrc: ''
    },
    "order": [[ 0, "desc" ]],
    columns: [
      { data: "campaign_id" },
      { data: "vendor_id" },
      { data: "quantity" },
      { data: "total_cost" },
      { data: "data_type" },
      { data: "envelope_type" },
      { data: "offer" },
      { data: "send_date" }
    ]
  }); 
}

dashboard.RegisterSubmitNewCampaign = function () {
  $('#new-campaign-form').submit(function(event){
    let $form = $(this);
    $('#new-campaign-form [type="submit"],#new-campaign-form [type="reset"]').addClass('disabled').attr('disabled','disabled');
    // 
    requestData = {};
    $.each($form.serializeArray(), function(i, v) {
      requestData[v.name] = v.value;
      if (v.name === "quantity") {
        requestData[v.name] = parseInt(requestData[v.name]);
      } else if (
        v.name === "cost_for_courier" || v.name === "cost_for_data" ||
        v.name === "cost_for_envelopes" || v.name === "cost_for_paper" ||
        v.name === "cost_for_postage" || v.name === "cost_for_strapping" ||
        v.name === "cost_for_labels" || v.name === "cost_for_labor"
      ) {
        requestData[v.name] = parseFloat(requestData[v.name]);
      } else if ( v.name === "send_date" ) {
        requestData[v.name] = requestData[v.name].replace("_", "0");
      }
    });
    // 
    $.ajax({
      url: '/api/campaign',
      type: 'post',
      dataType: 'json',
      contentType: "application/json; charset=utf-8",
      data: JSON.stringify(requestData)
    }).done(function(res) {
      if (res.status === true) {
        dashboard.campaignTable.ajax.reload();
        $form[0].reset();
      }
    }).fail(function(){
      // wrong
    }).always(function(){
      $('#new-campaign-form [type="submit"],#new-campaign-form [type="reset"]').removeClass('disabled').removeAttr('disabled');
    });

    event.preventDefault();
  });

}

// for upload
dashboard.RegisterForButtonImportData = function () {
  $('#import-excel-button').click(function(){
    $('#import-excel-input-file').trigger('click')
  });
  $('#import-excel-input-file').change(function(event){
    var tmppath = URL.createObjectURL(event.target.files[0]);
    $('#import-excel-file-path').attr('value', tmppath); // show file name
    $('#import-excel-status').html( event.target.files[0].name ); // show file name
    $('#import-excel').trigger('submit');
  });
};

dashboard.RegisterForSubmitImportData = function() {
  $('#import-excel').submit(function(event) {
    $form = $(this);
    // let formData = new FormData();
    // formData.append('file', this.file);

    $.ajax({
      url: '/api/excel/make_report',
      type: 'post',
      cache: false,
      contentType: false,
      processData: false,
      data: new FormData($form[0]),
    }).done(function(res) {
      // something done
      if (res.status) {
        $('#import-excel-status').html("Export result success!.");
      } else {
        $('#import-excel-status').html("An error has occurred, please try again!.");
      }
    }).fail(function(){
      // wrong
      $('#import-excel-status').html("An error has occurred, please try again!.");
    }).always(function(){
      $form[0].reset();
      // $('#import-excel-status').html("&nbsp;");
    });

    event.preventDefault();
  });
};

$(function() {
  dashboard.RegisterDateMask();
  dashboard.RegisterCampaignTable();
  dashboard.RegisterSubmitNewCampaign();
  // 
  dashboard.RegisterForSubmitImportData();
  dashboard.RegisterForButtonImportData();
});