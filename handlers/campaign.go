package handlers

import (
	"encoding/json"
	"fmt"
  "github.com/astaxie/beego"
  "bitbucket.org/trinhquocviet/gpm/database"
  "bitbucket.org/trinhquocviet/gpm/models"
)

// CampaignHandler is struct
type CampaignHandler struct {
  beego.Controller
}

// Get will get all campaign
func (c *CampaignHandler) Get() {
  c.EnableRender = false
  data, err := database.GetAllCampaign()
  if err != nil {
    fmt.Println(err)
  }
  c.Data["json"] = data
  c.ServeJSON(false)
}

// Post allow create new campaign
func (c *CampaignHandler) Post() {
  c.EnableRender = false

  requestData := models.Campaign{}
  err := json.Unmarshal(c.Ctx.Input.RequestBody, &requestData)
  requestData.CalculateTotalCost()
  if err != nil {
    fmt.Println(err)
    c.Data["json"] = map[string]bool{"status": false}
  }

  err = database.InsertCampaign(&requestData)
  if err != nil {
    fmt.Println(err)
    c.Data["json"] = map[string]bool{"status": false}
  }
  
  c.Data["json"] = map[string]bool{"status": true}
  c.ServeJSON(false)
}