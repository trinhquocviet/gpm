package handlers

import (
	"fmt"
  "github.com/astaxie/beego"
  // "os"
  "time"
  
  "bitbucket.org/trinhquocviet/gpm/models"
  "bitbucket.org/trinhquocviet/gpm/utils"
  "bitbucket.org/trinhquocviet/gpm/services/excel"
)

// ExcelHandler is struct
type ExcelHandler struct {
  beego.Controller
}

// MakeReport is Post request to import data from excel file and export report
func (c *ExcelHandler) MakeReport() {
  c.EnableRender = false
  var err error

  // build defer when end of API will return value
  defer func () {
    var responseData = &models.APIResponse { Status : err == nil, Message : "", }
    if err != nil {
      fmt.Println(err)
      responseData.Message = err.Error()
    }
    
    c.Data["json"] = &responseData
    c.ServeJSON(false)
  }()
  
  // 
  _, fileHeader, err := c.GetFile("file")
  
  if err == nil {
    reportDirectory := "report__" + time.Now().Format("2006-01-02 03h04m05s PM")
    err = utils.MakeDirectoryFromProject(reportDirectory)

    if err == nil {
      importFileURL, err := utils.GetLocalUploadPath( reportDirectory +"/"+ utils.ChangeFileNameKeepExt(fileHeader.Filename, "origin_data"))
      reportFileURL, err := utils.GetLocalUploadPath( reportDirectory +"/"+ utils.ChangeFileNameKeepExt(fileHeader.Filename, "report"))
      
      if err == nil {
        err = c.SaveToFile("file", importFileURL)
        err = excel.MakeReport(importFileURL, reportFileURL)
        // err = os.Remove(importFileURL)
      }
    }
  }

  

}