package utils

// GetExcelColumnByIndex will help to get Excel column name by index, index start from 0
func GetExcelColumnByIndex(index int) string {
  const LETTERS string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  var result = string("")
  
  var curPos = index
  if (index > 25) {
    curPos = 0
    var nextPos = index - 26
    result += GetExcelColumnByIndex(nextPos)
  }
  
  result += string(LETTERS[curPos])

  return result
}