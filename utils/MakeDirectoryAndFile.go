package utils

import (
  "path/filepath"
  "os"
)

// MakeDirectory will help to create directory
func MakeDirectoryFromProject(path string) error {
  dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return err
  }

  return os.Mkdir(filepath.Join(dir, path), 0777)
}