package utils

import (
  "path/filepath"
  "os"
)

// GetLocalUploadPath will allow get path to save name
func GetLocalUploadPath(filename string) (string, error) {
  result := ""

  dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return result, err
  }

  result = filepath.Join(dir, filename)
  return result, nil
}