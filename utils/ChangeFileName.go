package utils

import (
	"strings"
)

// ChangeFileNameKeepExt will replace name of file
func ChangeFileNameKeepExt(fileName string, newName string) string {
  strArray := strings.Split(fileName, ".")
  return newName + "." + strArray[len(strArray)-1]
}