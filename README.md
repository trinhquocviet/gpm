# GENERAL PROGRAM MAPPING

Using [Golang](https://golang.org/) and use cross platform compiling of Golang to create program and use [BoltDB](https://github.com/boltdb/bolt).

## Getting Started

| Required |  | Version  |
| :---: |:---:| :---|
| [Golang](https://golang.org/) | >= | 1.9 |
| [Glide](https://glide.sh/) | >= | 0.12.3 |
| [Beego](https://beego.me/) | >= | 1.9.0 |
| [BeeTool](https://github.com/beego/bee) | >= | 1.4.1 |
| [AdminLTE](https://adminlte.io/) | == | 2.3.11 |


### Prerequisites

What things you need to install the software and how to install them

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo
