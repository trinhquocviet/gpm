package main

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"

	_ "bitbucket.org/trinhquocviet/gpm/routers"
	"github.com/astaxie/beego"
	// "github.com/skratchdot/open-golang/open"
)

func main() {
  dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		fmt.Println(err)
  }
  
  fmt.Println("--- START GPM")
	fmt.Println("OS: " + runtime.GOOS)
  fmt.Println("Architecture: " + runtime.GOARCH)
  fmt.Println("Path: " + dir)

	beego.BConfig.WebConfig.ViewsPath = filepath.Join(dir, "views")

	// open.Start("http://localhost:" + beego.AppConfig.String("httpport"))
	beego.Run()
}
