package excel

import (
	"fmt"
	// "os"
  "io/ioutil"
  // "strconv"
  // "fmt"
  // "github.com/xuri/excelize"
  "github.com/tealeg/xlsx"
  // "bitbucket.org/trinhquocviet/gpm/utils"
  "bitbucket.org/trinhquocviet/gpm/database"
  "bitbucket.org/trinhquocviet/gpm/models"
)

// MakeReport will read data from excel file and import to database
func MakeReport(importFileURL string, reportFileURL string) error {
  var err error
  var reportFile *xlsx.File
  err = copyOriginToReportFile(importFileURL, reportFileURL)
  if err != nil {
    return err
  }

  reportFile, err = xlsx.OpenFile(reportFileURL)
  if err != nil {
    return err
  }

  defer func() {
    reportFile.Save(reportFileURL)
  }()

  /** */
  // 1. export all campaign to excel
  // 1. join
  err = exportCampaignToExcelSheet(reportFile)

  
  
  return err
}

func copyOriginToReportFile(src string, dest string) error {
  data, err := ioutil.ReadFile(src)
  if err != nil { return err }
  // Write data to dst
  err = ioutil.WriteFile(dest, data, 0644)
  if err != nil { return err }

  return nil
}


func exportCampaignToExcelSheet(reportFile *xlsx.File) error {
  var err error
  sheetName := "Campaigns"
  sheet, err := reportFile.AddSheet(sheetName)
  // 01. build column
  // 02. get data
  // 03. run loop to fill
  if err == nil {
    // 01. build column
    row := sheet.AddRow() 
    _ = row.WriteSlice(models.GetCampaignExcelColumnStructure(), -1)

    // 02. get data
    campaigns, err := database.GetAllCampaign()
    if err == nil {
      // 03. run loop to fill
      for index, campaign := range *campaigns {
        row = sheet.AddRow()
        campaignExcel := campaign.GetCampaignExcelStructure()
        campaignExcel.No = index + 1
        _ = row.WriteStruct(&campaignExcel, -1)
      }
    }
  }

  fmt.Println(err)
  return err
}


// func setFreezeTopPanes() {

// }



