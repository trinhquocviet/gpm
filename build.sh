#!/bin/bash
echo "Start build for MacOS and Windows"
echo "==="
echo "Remove all old file"
rm -rf ./dist
mkdir ./dist
echo "==="
echo "Building Mac 386"
GOOS=darwin GOARCH=386 go build -o ./dist/gpm_x86 main.go
echo "Building Mac amd64"
GOOS=darwin GOARCH=amd64 go build -o ./dist/gpm_amd64 main.go
echo "Building Windows 386"
GOOS=windows GOARCH=386 go build -o ./dist/gpm_x86.exe main.go
echo "Building Windows amd64"
GOOS=windows GOARCH=amd64 go build -o ./dist/gpm_amd64.exe main.go
echo "==="
echo "Copy config, views and static"
cp -R ./conf ./dist/conf
cp -R ./views ./dist/views
cp -R ./static ./dist/static
echo "Allmost done!!!"