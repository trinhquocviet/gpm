package routers

import (
  "bitbucket.org/trinhquocviet/gpm/controllers"
  "bitbucket.org/trinhquocviet/gpm/handlers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.DashboardController{})

    // list router for API
    beego.Router("/api/campaign", &handlers.CampaignHandler{}, "get:Get;post:Post")
    beego.Router("/api/excel/make_report", &handlers.ExcelHandler{}, "post:MakeReport")
}
