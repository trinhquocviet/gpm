package controllers

import (
  "github.com/astaxie/beego"
)

// DashboardController struct for dashboard
type DashboardController struct {
	beego.Controller
}

// Get will show page
func (c *DashboardController) Get() {
  c.Data["PageTitle"] = "Dashboard"
  c.Data["PageDescription"] = ""
  c.Layout = "layout/layout.html"
  c.TplName = "dashboard.html"
}
