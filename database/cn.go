package database

import (
	"os"
	"path/filepath"

	"github.com/boltdb/bolt"
)

// OpenConnection using to create & open connection to database.
// Database using is bolt, database create in the same directory.
// It returns the pointer *bolt.DB, you should call Close() after using this.
func openConnection() (*bolt.DB, error) {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
    return nil, err
	}
	// Open the my.db data file in your current directory.
	// It will be created if it doesn't exist.
	// Database should create in same directory with application
	db, err := bolt.Open(filepath.Join(dir, "gpm.db"), 0600, nil)
	if err != nil {
    return nil, err
	}

	return db, nil
}
