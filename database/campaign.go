package database

import (
	"strconv"
	"errors"
	"time"
	"encoding/json"
  "bitbucket.org/trinhquocviet/gpm/models"
  "github.com/boltdb/bolt"
)

// InsertCampaign will insert new campaign into database.
// It will send error if have any problem.
func InsertCampaign(data *models.Campaign) error {
  db, err := openConnection()
  if err != nil {
    return err
  }
  defer db.Close()
  
  db.Update(func(tx *bolt.Tx) error {
    b, err := tx.CreateBucketIfNotExists([]byte("campaigns"))
    if err != nil {
      return err
    }
    data.CreatedAt = time.Now()
    data.ID = strconv.Itoa(int(time.Now().Unix()))
    encoded, err := json.Marshal(data)
    if err != nil {
        return err
    }
    return b.Put([]byte(data.ID), encoded)
  })

  return nil
}

// GetAllCampaign Will return all campaign
func GetAllCampaign() (*[]models.Campaign, error) {
  results := make([]models.Campaign, 0)
  db, err := openConnection()
  if err != nil {
    return &results, err
  }
  defer db.Close()

  err = db.View(func(tx *bolt.Tx) error {
    b := tx.Bucket([]byte("campaigns"))
    if (b == nil) {
      return errors.New("bucket do not exists")
    }
    c := b.Cursor()
    for k, v := c.First(); k != nil; k, v = c.Next() {
      item := models.Campaign{}
      err = json.Unmarshal(v, &item)
      if err != nil {
        return err
      }
      results = append(results, item)
    }
  
    return nil
  })

  if err != nil {
    return &results, err
  }

  return &results, nil
}