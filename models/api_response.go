package models

// APIResponse is a basic response base on apiResponseBase
type APIResponse struct {
  Status  bool   `json:"status"`
	Message string `json:"message"`
}