package models

import (
	"time"
)

// Campaign will help mapping data from API and database.
type Campaign struct {
	ID         string `json:"id"`
	CampaignID string `json:"campaign_id"`
	VendorID   string `json:"vendor_id"`
	Quantity   int    `json:"quantity"`
	//
	TotalCost        float64 `json:"total_cost"`
	CostForCourier   float64 `json:"cost_for_courier"`
	CostForData      float64 `json:"cost_for_data"`
	CostForEnvelopes float64 `json:"cost_for_envelopes"`
	CostForPaper     float64 `json:"cost_for_paper"`
	CostForPostage   float64 `json:"cost_for_postage"`
	CostForStrapping float64 `json:"cost_for_strapping"`
	CostForLabels    float64 `json:"cost_for_labels"`
	CostForLabor     float64 `json:"cost_for_labor"`
	//
	DataType     string    `json:"data_type"`
	EnvelopeType string    `json:"envelope_type"`
	Offer        string    `json:"offer"`
	SendDate     string    `json:"send_date"`
	CreatedAt    time.Time `json:"created_at"`
}

// CampaignExcel will mapping with Campaign and use to insert to excel
type CampaignExcel struct {
	No           int     `xlsx:"0"`
	CampaignID   string  `xlsx:"1"`
	VendorID     string  `xlsx:"2"`
	Quantity     int     `xlsx:"3"`
	TotalCost    float64 `xlsx:"4"`
	DataType     string  `xlsx:"5"`
	EnvelopeType string  `xlsx:"6"`
	Offer        string  `xlsx:"7"`
	SendDate     string  `xlsx:"8"`
}

// CalculateTotalCost will calculate total cost
func (c *Campaign) CalculateTotalCost() {
	c.TotalCost = 0
	c.TotalCost += c.CostForCourier
	c.TotalCost += c.CostForData
	c.TotalCost += c.CostForEnvelopes
	c.TotalCost += c.CostForPaper
	c.TotalCost += c.CostForPostage
	c.TotalCost += c.CostForStrapping
	c.TotalCost += c.CostForLabels
	c.TotalCost += c.CostForLabor
}

// GetCampaignExcelStructure will return data type to import to excel
func (c *Campaign) GetCampaignExcelStructure() CampaignExcel {
	return CampaignExcel{
		No:           0,
		CampaignID:   c.CampaignID,
		VendorID:     c.VendorID,
		Quantity:     c.Quantity,
		TotalCost:    c.TotalCost,
		DataType:     c.DataType,
		EnvelopeType: c.EnvelopeType,
		Offer:        c.Offer,
		SendDate:     c.SendDate,
	}

}

func GetCampaignExcelColumnStructure() *[]string {
	result := []string{
		"No",
		"Campaign ID",
		"Vendor ID",
		"Quantity",
		"Total Cost",
		"Data Type",
		"Envelope Type",
		"Offer",
		"Send Date",
	}

	return &result
}
